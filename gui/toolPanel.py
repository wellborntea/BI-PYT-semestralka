from tkinter import *
from operace_img import operace as op

class toolPanel(Toplevel):
    def __init__(self,parent,top,title=None):
        Toplevel.__init__(self,parent)
        self.transient(parent)
        if title:
            self.title(title)
        self.top = top
        self.parent = parent
        self.image = self.top.image_shown   
        self.protocol("WM_DELETE_WINDOW", self.hide)
        self.buttonbox()
        
    def hide(self,event=None):
        self.top.panel_shown.set(0)
        self.show_image()
        self.destroy()
    
    def buttonbox(self):
        gs_frame = Frame(self)
        gs_button = Checkbutton(gs_frame,variable=self.top.gs_var)
        gs_label = Label(gs_frame,text="Grayscale")
        gs_button.grid(row=0,column=0)
        gs_label.grid(row=0,column=1)
        
        neg_frame = Frame(self)
        neg_button = Checkbutton(neg_frame,variable=self.top.neg_var)
        neg_label = Label(neg_frame,text="Negativ")
        neg_button.grid(row=0,column=0)
        neg_label.grid(row=0,column=1)
    
        light_frame = Frame(self)
        light_button = Checkbutton(light_frame,variable=self.top.light_var)
        light_label = Label(light_frame,text="Světlost")
        light_choice_frame = Frame(self)
        light_label2 = Label(light_choice_frame,text="%")
        self.light = light_entry = Entry(light_choice_frame,width=4,justify=RIGHT)
        light_entry.insert(0,self.top.light_val)
        light_button.grid(row=0,column=0)
        light_label.grid(row=0,column=1)
        light_entry.grid(row=0,column=0)
        light_label2.grid(row=0,column=1)
     
        edges_frame = Frame(self)
        edges_button = Checkbutton(edges_frame,variable=self.top.edges_var)
        edges_label = Label(edges_frame,text="Zobrazit hrany")
        edges_choice_frame = Frame(self)  
        edges_mask1 = Radiobutton(edges_choice_frame,text="1",variable=self.top.edges_mask,value=1)
        edges_mask2 = Radiobutton(edges_choice_frame,text="2",variable=self.top.edges_mask,value=2)
        edges_mask3 = Radiobutton(edges_choice_frame,text="3",variable=self.top.edges_mask,value=3)
        edges_button.grid(row=0,column=0)
        edges_label.grid(row=0,column=1)
        edges_mask1.grid(row=0,column=0)
        edges_mask2.grid(row=0,column=1)
        edges_mask3.grid(row=0,column=2)
        
        sharp_frame = Frame(self)
        sharp_button = Checkbutton(sharp_frame,variable=self.top.sharp_var)
        sharp_label = Label(sharp_frame,text="Zostřit") 
        sharp_choice_frame = Frame(self)  
        sharp_mask1 = Radiobutton(sharp_choice_frame,text="1",variable=self.top.sharp_mask,value=1)
        sharp_mask2 = Radiobutton(sharp_choice_frame,text="2",variable=self.top.sharp_mask,value=2)
        sharp_mask3 = Radiobutton(sharp_choice_frame,text="3",variable=self.top.sharp_mask,value=3)
        sharp_mask4 = Radiobutton(sharp_choice_frame,text="4",variable=self.top.sharp_mask,value=4)
        sharp_button.grid(row=0,column=0)
        sharp_label.grid(row=0,column=1)
        sharp_mask1.grid(row=0,column=0)
        sharp_mask2.grid(row=0,column=1)
        sharp_mask3.grid(row=0,column=2)
        sharp_mask4.grid(row=0,column=3)
        
        emb_frame = Frame(self)
        emb_button = Checkbutton(emb_frame,variable=self.top.emb_var)
        emb_label = Label(emb_frame,text="Vytepat")   
        emb_choice_frame = Frame(self)
        emb_mask1 = Radiobutton(emb_choice_frame,text="1",variable=self.top.emb_mask,value=1)
        emb_mask2 = Radiobutton(emb_choice_frame,text="2",variable=self.top.emb_mask,value=2)
        emb_button.grid(row=0,column=0)
        emb_label.grid(row=0,column=1)
        emb_mask1.grid(row=0,column=0)
        emb_mask2.grid(row=0,column=1)
        
        mask_label = Label(self,text="Typ masky")
        exec_button = Button(self,text="Použít",command=self.execute)
        reset_button = Button(self,text="Reset",command=self.reset)
        self.show_button = Button(self,text="Zobrazit původní",command=self.show_orig)
        self.bind("<Return>",self.execute)
        self.bind("<Escape>",self.hide)
        
        gs_frame.grid(row=1,column=0,sticky=W)
        neg_frame.grid(row=0,column=0,sticky=W)
        light_frame.grid(row=2,column=0,sticky=W)
        light_choice_frame.grid(row=2,column=1,sticky=W)
        mask_label.grid(row=3,column=1)
        sharp_frame.grid(row=4,column=0,sticky=W)
        sharp_choice_frame.grid(row=4,column=1)
        edges_frame.grid(row=5,column=0,sticky=W)
        edges_choice_frame.grid(row=5,column=1)
        emb_frame.grid(row=6,column=0,sticky=W)
        emb_choice_frame.grid(row=6,column=1)
        exec_button.grid(row=7,column=0,sticky=EW)
        reset_button.grid(row=7,column=1,sticky=EW)
        self.show_button.grid(row=8,columnspan=2,sticky=EW)
                      
    def show_image(self):
        self.top.image_shown = self.image
        self.top.show_image(self.top.image_shown)
        
    def grayscale(self):
        self.image = op.grayscale(self.image)
    
    def negativ(self):
        self.image = op.negativ(self.image)
         
    def lighting(self):
        self.top.light_val = float(self.light.get())
        self.image = op.lighting(self.image,self.top.light_val/100)
   
    def edges(self):
        self.image = op.edges(self.image,self.top.edges_mask.get())
        
    def sharpen(self):
        self.image = op.sharpen(self.image,self.top.sharp_mask.get())
        
    def emboss(self):
        self.image = op.emboss(self.image,self.top.emb_mask.get())
    
    def show_orig(self):
        self.show_button.config(text='Zobrazit změněný',command=self.show_changed)
        self.top.image_shown = self.top.image_orig
        self.top.show_image(self.top.image_shown)
        
    def show_changed(self):
        self.show_button.config(text='Zobrazit původní',command=self.show_orig)
        self.show_image()
        
    def execute(self,*args):
        self.image = self.top.image_orig
        if self.top.gs_var.get() == 1:
            self.grayscale()
        if self.top.neg_var.get() == 1:
            self.negativ()
        if self.top.light_var.get() == 1:
            self.lighting()
        if self.top.sharp_var.get() == 1:
            self.sharpen()
        if self.top.edges_var.get() == 1:
            self.edges()
        if self.top.emb_var.get() == 1:
            self.emboss()
        self.show_button.config(text='Zobrazit původní',command=self.show_orig)
        self.show_image()

    def reset(self,*args):
        self.top.light_val = 100
        self.light.delete(0,len(self.light.get()))
        self.light.insert(0,self.top.light_val)
        self.top.edges_mask.set(1)
        self.top.sharp_mask.set(1)
        self.top.emb_mask.set(1)
        self.top.gs_var.set(0)
        self.top.neg_var.set(0)
        self.top.light_var.set(0)
        self.top.edges_var.set(0)
        self.top.sharp_var.set(0)
        self.top.emb_var.set(0)
        self.execute()
        
