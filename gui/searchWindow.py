from tkinter import *
from tkinter import filedialog

def SearchWindow(parent,mode):
    file_opt = options = {}
    options['defaultextension'] = '.jpg'
    options['filetypes'] = [('Obrázky', '.jpg .jpeg .png .ppm .pgm .pnm .pbm'),('Všechny soubory', '.*')]
    options['initialdir'] = 'C:\\'
    options['initialfile'] = ''
    options['parent'] = parent
    if mode == 'r':
        options['title'] = 'Otevřít...'
        return filedialog.askopenfilename(**file_opt)
    if mode == 'w':
        options['title'] = 'Uložit jako...'
        return filedialog.asksaveasfilename(**file_opt)
            
