from tkinter import *
from tkinter import simpledialog
from PIL import Image, ImageTk

class resizeDialog(simpledialog.Dialog):
    def __init__(self,parent,title=None,init_val=100):
        self.value = init_val
        simpledialog.Dialog.__init__(self,parent,title)   
        
    def body(self,master):
        self.scale = Scale(master,from_=10,to=100,resolution=10,orient=HORIZONTAL)
        self.label = Label(master,text="\n%")
        self.scale.set(self.value)
        self.scale.grid(row=0,columnspan=2)
        self.label.grid(row=0,column=2)
        return
        
    def buttonbox(self):
        box = Frame(self)

        w = Button(box, text="OK", width=7, command=self.ok, default=ACTIVE)
        w.grid(row=1,column=0)
        w = Button(box, text="Zrušit", width=7, command=self.cancel)
        w.grid(row=1,column=2)

        self.bind("<Return>",self.ok)
        self.bind("<Escape>",self.cancel)

        box.pack()
        
    def apply(self):
        self.value = self.scale.get()
        
