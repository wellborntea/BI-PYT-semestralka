from tkinter import *
from gui.searchWindow import SearchWindow
from gui.resizeDialog import resizeDialog
from gui.toolPanel import toolPanel
from PIL import Image, ImageTk

class MainWindow:
    """
    Hlavni okno aplikace
    """
    def __init__(self):
        """
        Inicializuje prazdne okno bez obrazku.
        """
        self.root = Tk()
        self.root.title("Aplikace")
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        hlavniMenu = self.hlavniMenu = Menu(self.root)
        self.image_orig = self.picture = self.canvas = None
        self.panel_shown = IntVar()
        self.varInit()
        souborMenu = Menu(hlavniMenu,tearoff=0)
        souborMenu.add_command(label="Otevřít",command=self.open_)
        souborMenu.add_command(label="Uložit",command=self.save_)
        souborMenu.add_separator()
        souborMenu.add_command(label="Zavřít",command=self.root.quit)
        upravyMenu = Menu(hlavniMenu,tearoff=0)
        upravyMenu.add_command(label="Velikost zobrazení",command=self.resize_)
        upravyMenu.add_checkbutton(label="Panel nástrojů",variable=self.panel_shown,command=self.panel_switch)
        hlavniMenu.add_cascade(label="Soubor",menu=souborMenu)
        hlavniMenu.add_cascade(label="Úpravy",menu=upravyMenu)
        hlavniMenu.entryconfig("Úpravy",state="disabled")
        self.root.config(menu=hlavniMenu)
    
    def varInit(self):
        self.edges_mask = IntVar()
        self.sharp_mask = IntVar()
        self.emb_mask = IntVar()
        self.gs_var = IntVar()
        self.neg_var = IntVar()
        self.light_var = IntVar()
        self.edges_var = IntVar()
        self.sharp_var = IntVar()
        self.emb_var = IntVar()
        
    def varSet(self):   
        self.light_val = 100
        self.edges_mask.set(1)
        self.sharp_mask.set(1)
        self.emb_mask.set(1)
        self.gs_var.set(0)
        self.neg_var.set(0)
        self.light_var.set(0)
        self.edges_var.set(0)
        self.sharp_var.set(0)
        self.emb_var.set(0)
         
    def open_(self):
        """
        Zobrazi nabidku pro nacteni obrazku, vybrany obrazek pak zobrazi. Nastaví proměnné operací na počáteční hodnotu. 
        """
        openFilePath = SearchWindow(self.root,mode='r')
        if len(openFilePath) != 0:
            if self.image_orig == None:
                self.hlavniMenu.entryconfig("Úpravy",state="normal")
            self.image_shown = self.image_orig = Image.open(openFilePath)
            self.width, self.height = self.__resize__()
            self.size_perc = 100
            self.varSet()
            self.root.title(openFilePath)
            self.show_image(self.image_shown)
    
    def save_(self):
        """
        Zobrazi nabidku pro ulozeni obrazku, a pokud byla zvolena cesta, tak obrazek ulozi.
        """
        if self.image_orig != None:
            saveFilePath = SearchWindow(self.root,mode='w')
            if len(saveFilePath) != 0:
                self.image_shown.save(saveFilePath)

    def show_image(self,image):
        """
        Zobrazi obrazek v hlavnim okne v zadane velikosti.
        """
        width = int(self.width*(self.size_perc/100))
        height = int(self.height*(self.size_perc/100))  
        self.tkimage = ImageTk.PhotoImage(image.resize((width,height)))
        if self.canvas == None:
            self.canvas = Canvas(self.root,width=width,height=height)
            self.picture = self.canvas.create_image((width,height),image=self.tkimage,anchor=SE)
            self.canvas.grid(row=0,column=0)
        else:
            self.canvas.delete(self.picture)
            self.picture = self.canvas.create_image((width,height),image=self.tkimage,anchor=SE)
            self.canvas.config(width=width,height=height)
            
    def __resize__(self):
        """
        Vraci velikost obrazku. Pokud je obrazek vetsi nez displej, zobrazeni se zmensi na 0.85*velikost displeje.
        """
        width, height = self.image_orig.size[0:2]
        size_koef = 0.85
        if width > self.sw*size_koef:
            width = int(self.sw*size_koef)
            height = int(height*(self.sw/width)*size_koef)
        if height > self.sh*size_koef:
            height = int(self.sh*size_koef)
            width = int(width*(self.sh/height)*size_koef)
        return width, height
        
    def resize_(self):
        rs = resizeDialog(self.root,title="Změnit velikost",init_val=self.size_perc)
        self.size_perc = rs.value
        self.show_image(self.image_shown)
        
    def panel_switch(self):
        if self.panel_shown.get() == 1:
            self.tp = toolPanel(self.root,self,"Panel nástrojů")
            self.tp.protocol("WM_DELETE_WINDOW", self.hideTP)
        else:
            self.tp.hide()
            
    def hideTP(self):
        self.panel_shown.set(0)
        self.tp.hide()
        
