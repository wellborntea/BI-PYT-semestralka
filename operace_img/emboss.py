import numpy as np
from PIL import Image
from numba import jit
if __name__ == '__main__':
    from to_image import to_image
    import aplikace_masky as am
else:
    from operace_img.to_image import to_image
    from operace_img import aplikace_masky as am

def emboss(image, mask):
    """
    Emboss
    ======
    Provede vytlak obrazku podle zvolene masky.
    
    Pouziti
    -------
    emboss(image, mask):
        ...
        return out
    
    image - Pillow Image (cernobily nebo RGB)
    mask - cislo zvolene masky
    out - Pillow Image
    
    Masky
    -----
    1)  [-1,-1, 0]  (default)
        [-1, 0, 1]    
        [ 0, 1, 1]
    
    2)  [-1,-1,-1,-1, 0]
        [-1,-1,-1, 0, 1]
        [-1,-1, 0, 1, 1]
        [-1, 0, 1, 1, 1]
        [ 0, 1, 1, 1, 1]
    
    Priklady
    --------
    >>> img = Image.open('kvetina.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_emb = emboss(img,1)
    >>> data_emb = np.asarray(img_emb, dtype=np.float)
    >>> (375,500,3) == data.shape == data_emb.shape
    True
    
    >>> img = Image.open('kvetina_gray.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_emb = emboss(img,1)
    >>> data_emb = np.asarray(img_emb, dtype=np.float)
    >>> (375,500) == data.shape == data_emb.shape 
    True
    """
    data = np.asarray(image, dtype=np.float)
    factor = 1
    bias = 128
    if mask == 2:
        maska = np.array([
            [-1,-1,-1,-1, 0],
            [-1,-1,-1, 0, 1],
            [-1,-1, 0, 1, 1],
            [-1, 0, 1, 1, 1],
            [ 0, 1, 1, 1, 1],
        ])
        size_m = 5
    else:
        maska = np.array([
            [-1,-1, 0],
            [-1, 0, 1],    
            [ 0, 1, 1],
        ])
        size_m = 3
    data_w, data_h = data.shape[0:2]
    out = np.zeros([1])
    if len(data.shape) == 2:
        out = am.__cyklusL__(data,data_h,data_w,maska,size_m,factor,bias)
    elif len(data.shape) == 3:
        out = am.__cyklusRGB__(data,data_h,data_w,maska,size_m,factor,bias)
    out = np.clip(out, 0, 255); 
    return to_image(out)
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
