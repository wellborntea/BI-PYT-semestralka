from PIL import Image

def grayscale(image):
    """
    Grayscale
    =========
    Prevede obrazek do odstinu sedi.
    
    Pouziti
    -------
    grayscale(image):
        ...
        return out
    
    image - Pillow Image (libovolny typ)
    out - Pillow Image (cernobily)
    
    Priklady
    --------
    >>> img = Image.open('kvetina.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_gray = gray_scale(img)
    >>> data_gray = np.asarray(img_gray, dtype=np.float)
    >>> data.shape[0:2] == data_gray.shape
    True
    """
    
    out = image.convert('L')
    return out
    
if __name__ == '__main__':
    import doctest
    import numpy as np
    doctest.testmod()
