if __name__ == '__main__':
    from grayscale import grayscale
    from negativ import negativ
    from lighting import lighting
    from edges import edges
    from sharp import sharpen
    from emboss import emboss
else:
    from operace_img.grayscale import grayscale
    from operace_img.negativ import negativ
    from operace_img.lighting import lighting
    from operace_img.edges import edges
    from operace_img.sharp import sharpen
    from operace_img.emboss import emboss

def operace():    
    """
    Uprava obrazku
    ==============

    Podporovane operace
    -------------------

    grayscale(img)      - Prevod do odstinu sedi
    negativ(img)        - Inverze barev
    lighting(img,koef)  - Zmena svetlosti
    edges(img,mask)     - Hledani hran
    sharpen(img,mask)   - Zostreni obrazku
    emboss(img,mask)    - Vytlak obrazku
    """
    pass
