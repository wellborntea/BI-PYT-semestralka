import numpy as np
from PIL import Image

class UnknownMode(Exception):
    pass

def to_image(data):    
    if len(data.shape) == 2:
        out = Image.fromarray(np.asarray(data, dtype=np.uint8),'L')
    elif len(data.shape) == 3:
        out = Image.fromarray(np.asarray(data, dtype=np.uint8),'RGB')
    else:
        raise UnknownMode('Neznamy format obrazku')
    return out
