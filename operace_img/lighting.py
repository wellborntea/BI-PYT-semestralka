from PIL import Image
import numpy as np
if __name__ == '__main__':
    from to_image import to_image
else:
    from operace_img.to_image import to_image

def lighting(image, koef):
    """ 
    Lighting
    ========
    Zmeni svetlost obrazku.
    
    Pouziti
    -------
    lighting(image, koef):
        ...
        return out
    
    image - Pillow Image (cernobily nebo RGB)
    koef - koeficient osveleni > 1 --> zesvetleni obrazku
                               < 1 --> ztmaveni obrazku
    out - Pillow Image
    
    Priklady
    --------
    >>> img = Image.open('kvetina.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_light = lighting(img,2)
    >>> data_light = np.asarray(img_light, dtype=np.float)
    >>> (375,500,3) == data.shape == data_light.shape
    True
    
    >>> img = Image.open('kvetina_gray.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_light = lighting(img,2)
    >>> data_light = np.asarray(img_light, dtype=np.float)
    >>> (375,500) == data.shape == data_light.shape 
    True
    """
    
    data = np.asarray(image, dtype=np.uint16)
    data = data*koef
    data = np.clip(data, 0, 255)
    data = np.asarray(data, dtype=np.uint8)
    return to_image(data)
        
if __name__ == '__main__':
    import doctest
    doctest.testmod()
