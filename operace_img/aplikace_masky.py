from numba import jit
import numpy as np

@jit
def __cyklusRGB__(vstup,data_h,data_w,maska,size_m,factor=1,bias=0):
    vystup = np.zeros([data_w,data_h,3])
    for i in range(3):
        for y in range(size_m//2,data_h-size_m//2):
            for x in range(size_m//2,data_w-size_m//2):
                vyrez = vstup[x-size_m//2:x+size_m//2+1,y-size_m//2:y+size_m//2+1,i]
                vystup[x,y,i] = (vyrez*maska).sum()*factor+bias
    return vystup
    
@jit
def __cyklusL__(vstup,data_h,data_w,maska,size_m,factor=1,bias=0):
    vystup = np.zeros([data_w,data_h])
    for y in range(size_m//2,data_h-size_m//2):
        for x in range(size_m//2,data_w-size_m//2):
            vyrez = vstup[x-size_m//2:x+size_m//2+1,y-size_m//2:y+size_m//2+1]
            vystup[x,y] = (vyrez*maska).sum()*factor+bias
    return vystup

