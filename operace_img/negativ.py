from PIL import Image
import numpy as np
if __name__ == '__main__':
    from to_image import to_image
else:
    from operace_img.to_image import to_image

def negativ(image):
    """ 
    Negativ
    =======
    Vytvori z obrazku jeho negativ.
    
    Pouziti
    -------
    negativ(image):
        ...
        return out
    
    image - Pillow Image (cernobily nebo RGB)
    out - Pillow Image
    
    Priklady
    --------
    >>> img = Image.open('kvetina.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_neg = negativ(img)
    >>> data_neg = np.asarray(img_neg, dtype=np.float)
    >>> (375,500,3) == data.shape == data_neg.shape
    True
    
    >>> img = Image.open('kvetina_gray.jpg')
    >>> data = np.asarray(img, dtype=np.float)
    >>> img_neg = negativ(img)
    >>> data_neg = np.asarray(img_neg, dtype=np.float)
    >>> (375,500) == data.shape == data_neg.shape 
    True
    """
    
    data = np.asarray(image, dtype=np.float)
    data = 255 - data
    return to_image(data)
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
