Semestralni projekt pro predmet BI-PYT
======================================

Aplikace pro grafickou upravu obrazku
-------------------------------------
Program vyvijen na Ubuntu 16.04 v Pythonu 3.5.2.
Program se startuje spustenim souboru main.py.

Pouzivane knihovny
------------------
numpy;
Pillow - Image, ImageTk;
numba - jit;
tkinter;
